///
/// \file	bplaybook.cc
///		bplaybook command line tool
///

/*
    Copyright (C) 2011, Nicolas VIVIEN
    Copyright (C) 2005-2011, Net Direct Inc. (http://www.netdirect.ca/)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

    See the GNU General Public License in the COPYING file at the
    root directory of this project for more details.
*/


#include <barry/barry.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include "i18n.h"

#include "barrygetopt.h"

using namespace std;
using namespace Barry;


void Usage()
{
   int logical, major, minor;
   const char *Version = Barry::Version(logical, major, minor);

   cerr
   << "bplaybook - Command line USB Blackberry PlayBook\n"
   << "            Copyright 2011, Nicolas VIVIEN.\n"
   << "            Using: " << Version << "\n"
   << "\n"
   << "   -h        This help\n"
   << "   -v        Dump protocol data during operation\n"
   << "\n"
   << endl;
}


int main(int argc, char *argv[], char *envp[])
{
	INIT_I18N(PACKAGE);

	try {
		bool data_dump = false;
		vector<string> params;
		string iconvCharset;

		// process command line options
		for(;;) {
			int cmd = getopt(argc, argv, "hv");
			if( cmd == -1 )
				break;

			switch( cmd )
			{
			case 'v':	// data dump on
				data_dump = true;
				break;

			case 'h':	// help
			default:
				Usage();
				return 0;
			}
		}

		argc -= optind;
		argv += optind;

		if( argc != 0 ) {
			cerr << "unknown arguments" << endl;
			Usage();
			return 1;
		}

		// Initialize the barry library.  Must be called before
		// anything else.
		Barry::Init(data_dump);

		// Probe the USB bus for Blackberry devices and display.
		// If user has specified a PIN, search for it in the
		// available device list here as well
		Barry::ProbePlayBook probe;
	}
	catch( Usb::Error &ue) {
		std::cout << endl;	// flush any normal output first
		std::cerr << "Usb::Error caught: " << ue.what() << endl;
		return 1;
	}
	catch( Barry::Error &se ) {
		std::cout << endl;
		std::cerr << "Barry::Error caught: " << se.what() << endl;
		return 1;
	}
	catch( std::exception &e ) {
		std::cout << endl;
		std::cerr << "std::exception caught: " << e.what() << endl;
		return 1;
	}

	return 0;
}

